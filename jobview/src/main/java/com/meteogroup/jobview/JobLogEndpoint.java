package com.meteogroup.jobview;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

@WebSocket
public class JobLogEndpoint {

  @OnWebSocketMessage
  public void onMessage(String message) {
    System.out.println(message);
    LogManager.INSTANCE.log(message);
  }

  @WebSocket
  public static class LogEndpoint implements PropertyChangeListener {

    private final Set<Session> sessions = Collections
        .newSetFromMap(new ConcurrentHashMap<Session, Boolean>());

    public LogEndpoint() {
      System.out.println("Register");
      LogManager.INSTANCE.addPropertyChangeListener(this);
    }

    @OnWebSocketConnect
    public void onOpen(Session session) {
      System.out.println("On open");
      sessions.add(session);
    }

    @OnWebSocketClose
    public void onClose(Session session, int statuscode, String reason) {
      sessions.remove(session);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
      System.out.println("Fire event");
      for (Session session : sessions) {
        try {
          session.getRemote().sendString((String) evt.getNewValue());
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }
}