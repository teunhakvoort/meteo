package com.meteogroup.jobview;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.glassfish.tyrus.server.Server;

public class JobviewServer {

  /**
   * @param args
   */
  public static void main(String[] args) {
    new JobviewServer().runServer();
  }

  public void runServer() {

    Server server = new Server("localhost", 8025, "/websocket",
        JobLogEndpoint.class);

    try {
      server.start();
      BufferedReader reader = new BufferedReader(new InputStreamReader(
          System.in));
      System.out.print("Please press a key to stop the server.");
      reader.readLine();
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      server.stop();
    }
  }

}
