package com.meteogroup.jobview;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.webapp.WebAppContext;
import org.eclipse.jetty.websocket.server.WebSocketHandler;
import org.eclipse.jetty.websocket.servlet.ServletUpgradeRequest;
import org.eclipse.jetty.websocket.servlet.ServletUpgradeResponse;
import org.eclipse.jetty.websocket.servlet.WebSocketCreator;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;

import com.meteogroup.jobview.JobLogEndpoint.LogEndpoint;

public class WebSocketServer {
  private Server server;
  private String host;
  private int port;
  private List<Handler> webSocketHandlerList = new ArrayList<>();

  public static void main(String[] args) throws Exception {
    WebSocketServer webSocketServer = new WebSocketServer();
    webSocketServer.setHost("localhost");
    webSocketServer.setPort(8025);
    webSocketServer.addWebSocket("/websocket", JobLogEndpoint.class);
    webSocketServer.initialize();
    webSocketServer.start();
  }

  public void initialize() {
    server = new Server();
    ServerConnector connector = new ServerConnector(server);
    connector.setSoLingerTime(-1);
    connector.setPort(port);
    server.addConnector(connector);

    WebAppContext webAppContext = new WebAppContext();
    webAppContext.setServer(server);
    webAppContext.setWar("src/main/webapp");
    webAppContext.setContextPath("/jetty");

    HandlerCollection handlerCollection = new HandlerCollection();
    handlerCollection.addHandler(webAppContext);
    for (Handler h : webSocketHandlerList) {
      handlerCollection.addHandler(h);
    }
    server.setHandler(handlerCollection);
  }

  public void addWebSocket(String pathSpec, final Class<?> webSocket) {
    final JobLogEndpoint echoEndpoint = new JobLogEndpoint();
    final LogEndpoint logEndpoint = new LogEndpoint();
    WebSocketHandler wsHandler = new WebSocketHandler() {
      @Override
      public void configure(WebSocketServletFactory webSocketServletFactory) {
        webSocketServletFactory.setCreator(new WebSocketCreator() {

          @Override
          public Object createWebSocket(ServletUpgradeRequest req,
              ServletUpgradeResponse resp) {
            String requestURI = req.getRequestURI().toString();
            if (requestURI.endsWith("/log")) {
              System.out.println("/log");
              return logEndpoint;
            } else {
              System.out.println("/echo");
              return echoEndpoint;
            }
          }
        });
      }
    };
    ContextHandler wsContextHandler = new ContextHandler();
    wsContextHandler.setHandler(wsHandler);
    wsContextHandler.setContextPath(pathSpec);
    webSocketHandlerList.add(wsHandler);
  }

  public void start() throws Exception {
    server.start();
    server.join();
  }

  public void stop() throws Exception {
    server.stop();
    server.join();
  }

  public void setHost(String host) {
    this.host = host;
  }

  public void setPort(int port) {
    this.port = port;
  }

}