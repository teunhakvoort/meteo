package com.meteogroup.jobview;

import javax.servlet.annotation.WebServlet;

import org.eclipse.jetty.websocket.servlet.WebSocketServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;

@SuppressWarnings("serial")
@WebServlet(name = "MyEcho WebSocket Servlet", urlPatterns = { "/echo" })
public class EchoServlet extends WebSocketServlet {

  @Override
  public void configure(WebSocketServletFactory factory) {
    System.out.println("Configure: " + getClass().getName());
    factory.getPolicy().setIdleTimeout(10000);
    factory.register(JobLogEndpoint.class);
  }
}