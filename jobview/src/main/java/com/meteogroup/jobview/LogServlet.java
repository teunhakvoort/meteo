package com.meteogroup.jobview;

import javax.servlet.annotation.WebServlet;

import org.eclipse.jetty.websocket.servlet.WebSocketServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;

import com.meteogroup.jobview.JobLogEndpoint.LogEndpoint;

@SuppressWarnings("serial")
@WebServlet(name = "MyEcho WebSocket Servlet", urlPatterns = { "/log" })
public class LogServlet extends WebSocketServlet {

  @Override
  public void configure(WebSocketServletFactory factory) {
    System.out.println("Configure: " + getClass().getName());
    factory.getPolicy().setIdleTimeout(10000);
    factory.register(LogEndpoint.class);
  }
}