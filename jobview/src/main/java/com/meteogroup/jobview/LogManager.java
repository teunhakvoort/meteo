package com.meteogroup.jobview;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public enum LogManager {
  INSTANCE;

  private final PropertyChangeSupport support = new PropertyChangeSupport(this);

  public void addPropertyChangeListener(PropertyChangeListener listener) {
    support.addPropertyChangeListener(listener);
  }

  /**
   * 
   * @param message
   */
  public synchronized void log(String message) { // TODO anders oplossen dan
                                                 // synchronized
    support.firePropertyChange("message", null, message);
  }

}
