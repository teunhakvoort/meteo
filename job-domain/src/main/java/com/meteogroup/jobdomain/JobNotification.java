package com.meteogroup.jobdomain;

import org.joda.time.DateTime;

public final class JobNotification {

  private final String jobId;

  private final String nodeId;

  private final String user;

  private final DateTime moment;

  public JobNotification(String jobId, String nodeId, String user,
      DateTime moment) {
    super();
    this.jobId = jobId;
    this.nodeId = nodeId;
    this.user = user;
    this.moment = moment;
  }

  public String getJobId() {
    return jobId;
  }

  public String getNodeId() {
    return nodeId;
  }

  public String getUser() {
    return user;
  }

  public DateTime getMoment() {
    return moment;
  }

}
