package nl.meteogroup.multithreading.domain;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Context implements Serializable {

  private static final long serialVersionUID = 5894841566495565450L;

  private String startedBy;
  private final int rang;
  private final int numberOfProcessors;
  private List<Integer> nodeIds;
  private File dataDir;

  public Context(int rang, int numberOfProcessors, File dataDir,
      String startedBy) {
    super();
    this.rang = rang;
    this.numberOfProcessors = numberOfProcessors;
    this.dataDir = dataDir;
    this.startedBy = startedBy;

    List<Integer> nodeIds = new ArrayList<Integer>(numberOfProcessors - 1);
    for (int i = 1; i < numberOfProcessors; i++) {
      nodeIds.add(i);
    }
    this.nodeIds = Collections.unmodifiableList(nodeIds);
  }

  public String getStartedBy() {
    return startedBy;
  }

  public File getDataDir() {
    return dataDir;
  }

  public List<Integer> getNodeIds() {
    return nodeIds;
  }

  public int getRang() {
    return rang;
  }

  public int getNumberOfProcessors() {
    return numberOfProcessors;
  }

  public int getNumberOfNodes() {
    return numberOfProcessors - 1;
  }

}
