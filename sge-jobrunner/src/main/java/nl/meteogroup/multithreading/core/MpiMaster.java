package nl.meteogroup.multithreading.core;

import nl.meteogroup.multithreading.annotations.Log;
import nl.meteogroup.multithreading.domain.Context;

public interface MpiMaster {

  @Log
  public abstract void runMaster(Context context);

}
