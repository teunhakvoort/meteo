package nl.meteogroup.multithreading.core;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import javax.inject.Inject;
import javax.inject.Singleton;

import mpi.MPI;
import mpi.MPIException;
import nl.meteogroup.multithreading.domain.Message;
import nl.meteogroup.multithreading.domain.SendException;

@Singleton
public class Messager {

  @Inject
  JobLogger logger;

  public <T extends Serializable> void sendObjectToMaster(T object,
      Message message) throws SendException {
    sendObject(object, message, 0);
  }

  public <T extends Serializable> void sendObjectToNode(T object,
      Message message, int nodeId) throws SendException {
    logger.log("Send: " + message.getDescription() + " to node " + nodeId);
    sendObject(object, message, nodeId);
  }

  private <T extends Serializable> void sendObject(T object, Message message,
      int destination) throws SendException {
    ByteArrayOutputStream bos = new ByteArrayOutputStream();
    ObjectOutputStream oos;
    try {
      oos = new ObjectOutputStream(bos);
      oos.writeObject(object);
      oos.close();
      byte[] bytes = bos.toByteArray();

      MPI.COMM_WORLD.Send(new int[] { bytes.length }, 0, 1, MPI.INT,
          destination, message.getId());
      MPI.COMM_WORLD.Send(bytes, 0, bytes.length, MPI.BYTE, destination,
          message.getId());
    } catch (MPIException e) {
      throw new SendException();
    } catch (IOException e) {
      throw new SendException();
    }
  }

  public <T extends Serializable> T recieveObjectFromNode(Class<T> clazz,
      Message message, int nodeId) throws IOException, Exception {
    int[] longs = new int[1];
    MPI.COMM_WORLD.Recv(longs, 0, 1, MPI.INT, nodeId, message.getId());
    int length = longs[0];

    logger.log("Length: " + length);
    byte[] bytes = new byte[length];
    MPI.COMM_WORLD.Recv(bytes, 0, bytes.length, MPI.BYTE, nodeId,
        message.getId());

    ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(
        bytes));

    Object readedObject = ois.readObject();
    if (readedObject.getClass().equals(clazz)) {
      return (T) readedObject;
    } else {
      throw new Exception("Onjuist type");
    }
  }

  public <T extends Serializable> T recieveObject(Class<T> clazz,
      Message message) throws IOException, Exception {
    return recieveObjectFromNode(clazz, message, 0);
  }

}
