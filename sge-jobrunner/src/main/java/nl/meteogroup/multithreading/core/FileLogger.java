package nl.meteogroup.multithreading.core;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.enterprise.inject.Any;
import javax.inject.Inject;
import javax.inject.Singleton;

import nl.meteogroup.multithreading.domain.Context;

import com.google.common.io.Files;

@Singleton
@Decorator
public class FileLogger implements JobLogger {

  @Inject
  @Delegate
  @Any
  JobLogger logger;

  private Context context;

  private BufferedWriter writer;

  public FileLogger() {
    Runtime.getRuntime().addShutdownHook(new Thread() {
      public void run() {
        System.out.println("Run exit hook");
        if (writer != null) {
          try {
            writer.close();
            if (context.getRang() == 0) {
              File logFile = new File(context.getDataDir(), "total-"
                  + System.currentTimeMillis() + ".log");
              System.out.println("Write log to: " + logFile.getAbsolutePath());
              try (BufferedWriter writer = Files.newWriter(logFile,
                  Charset.defaultCharset())) {
                for (int node = 0; node < context.getNumberOfProcessors(); node++) {
                  File nodeFile = new File(context.getDataDir(), node + ".log");

                  writer.newLine();
                  writer.write("NODE " + node);
                  writer.write("-------------------------------------------");
                  writer.newLine();

                  BufferedReader reader = Files.newReader(nodeFile,
                      Charset.defaultCharset());
                  String line = null;
                  while ((line = reader.readLine()) != null) {
                    writer.write(" " + line);
                    writer.newLine();
                  }
                }
              }
            }
          } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
          }
        }
      }
    });
  }

  @Override
  public void log(String msg) {
    if (writer != null) {
      try {
        writer.newLine();
        writer.write(msg);
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
    logger.log(msg);
  }

  @Override
  public void setContext(Context context) {
    this.context = context;
    if (writer == null) {
      try {
        File logFile = new File(context.getDataDir(), context.getRang()
            + ".log");

        if (!logFile.exists()) {
          logFile.createNewFile();
          log("Logfile created at " + logFile.getAbsolutePath());
        } else {
          log("Use: " + logFile.getAbsolutePath());
        }
        writer = Files.newWriter(logFile, Charset.defaultCharset());
      } catch (FileNotFoundException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
    logger.setContext(context);
  }

}