package nl.meteogroup.multithreading.core;

import nl.meteogroup.multithreading.domain.Context;

public interface JobLogger {

  public void log(String msg);

  public void setContext(Context context);

}