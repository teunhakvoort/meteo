package nl.meteogroup.multithreading.core;

import java.io.IOException;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.inject.Singleton;
import javax.websocket.ContainerProvider;
import javax.websocket.DeploymentException;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

import nl.meteogroup.multithreading.annotations.ClientLoggerWs;
import nl.meteogroup.multithreading.domain.Context;

@Singleton
public class WebSocketLogger implements JobLogger {

  private static final Logger logger = Logger.getLogger("meteo");
  private final WebSocketContainer container = ContainerProvider
      .getWebSocketContainer();
  private Session session;

  private int niveaus = 0;
  private Context context;

  public WebSocketLogger() {
    connect();
  }

  public void decrement() {
    niveaus--;
  }

  public void increment() {
    niveaus++;
  }

  private void connect() {
    String uri = "ws://localhost:8025/websocket/echo";
    try {
      session = container
          .connectToServer(ClientLoggerWs.class, URI.create(uri));
      session.setMaxIdleTimeout(999999);
      log("Connect to server!");
    } catch (DeploymentException | IOException e) {
      e.printStackTrace();
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see nl.meteogroup.multithreading.core.Logger#log(java.lang.String)
   */
  @Override
  public void log(String msg) {
    StringBuilder builder = new StringBuilder();
    for (int i = 0; i < niveaus; i++) {
      builder.append(" ");
    }
    String message = builder.toString() + msg;
    logger.log(Level.INFO, message);
    try {
      if (!session.isOpen()) {
        connect();
      }
      session.getBasicRemote().sendText(message);
    } catch (Throwable t) {
      logger.log(Level.WARNING, "WebSocketError", t);
    }
  }

  @Override
  public void setContext(Context context) {
    this.context = context;
  }

  @Override
  protected void finalize() throws Throwable {
    super.finalize();
    if (session.isOpen()) {
      session.close();
    }
  }

}
