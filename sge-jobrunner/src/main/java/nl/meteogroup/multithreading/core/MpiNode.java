package nl.meteogroup.multithreading.core;

import nl.meteogroup.multithreading.annotations.Log;
import nl.meteogroup.multithreading.domain.Context;

public interface MpiNode {

  @Log
  public void runNode(Context context);

}
