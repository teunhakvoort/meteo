package nl.meteogroup.multithreading.core;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.inject.Inject;

import mpi.MPI;
import nl.meteogroup.multithreading.domain.Context;
import nl.meteogroup.multithreading.domain.Messages;
import nl.meteogroup.multithreading.domain.SendException;

import org.jboss.weld.environment.se.StartMain;
import org.jboss.weld.environment.se.bindings.Parameters;
import org.jboss.weld.environment.se.events.ContainerInitialized;

public class JobStarter {

  @Inject
  Messager messager;

  @Inject
  JobLogger logger;

  @Inject
  BeanManager beanManager;

  public Context createContext(List<String> args) {
    String jobId = System.getenv("JOB_ID");
    File dataDir;
    try {
      dataDir = new File(new File(".").getCanonicalFile(), getClass()
          .getSimpleName() + "/" + jobId);
      MPI.Init(args.toArray(new String[args.size()]));

      int myrank = MPI.COMM_WORLD.Rank();
      int size = MPI.COMM_WORLD.Size();
      if (myrank == 0) {
        createDataDir(dataDir);
      }
      Context context = new Context(myrank, size, dataDir,
          System.getProperty("user.name"));
      return context;
    } catch (IOException e) {
      throw new IllegalStateException(e);
    }
  }

  public void run(@Observes ContainerInitialized event,
      @Parameters List<String> args) throws Exception {
    Context context = createContext(args);
    String jobClassName = args.get(args.size() - 1);
    try {
      Class<?> jobClazz = Class.forName(jobClassName);

      logger.log(jobClazz.getName());
      MpiJobInfo jobInfo = jobClazz.getAnnotation(MpiJobInfo.class);

      logger.setContext(context);
      if (context.getRang() == 0) {
        logger.log("START " + jobClassName);
        Class<? extends MpiMaster> masterClazz = jobInfo.master();
        MpiMaster master = createBean(masterClazz);
        sendContextToNodes(context);
        master.runMaster(context);
      } else {
        Class<? extends MpiNode> nodeClazz = jobInfo.node();
        context = messager.recieveObject(Context.class, Messages.CONTEXT);
        logger.setContext(context);
        runNode(nodeClazz, context);
      }
    } finally {
      MPI.Finalize();
      logger.log("END " + jobClassName);
    }
  }

  private void runNode(Class<? extends MpiNode> nodeClazz, Context context)
      throws UnknownHostException {
    InetAddress addr = InetAddress.getLocalHost();
    String ipAddress = addr.getHostAddress();
    logger.log("Starting node " + context.getRang() + " - " + "Address: "
        + addr.getHostName() + "(" + ipAddress + ")");

    MpiNode node = createBean(nodeClazz);
    node.runNode(context);
  }

  private Context sendContextToNodes(Context context) throws SendException {
    logger
        .log("Starting job on " + context.getNumberOfProcessors() + " cores.");
    logger.log("Starting master " + context.getRang());

    for (Integer node : context.getNodeIds()) {
      Context nodeContext = new Context(node, context.getNumberOfProcessors(),
          context.getDataDir(), context.getStartedBy());
      messager.sendObjectToNode(nodeContext, Messages.CONTEXT, node);
    }
    return context;
  }

  private void createDataDir(File dataDir) {
    dataDir.mkdirs();
    if (dataDir.exists()) {
      logger.log("Datadir succesvol aangemaakt op: "
          + dataDir.getAbsolutePath());
    } else {
      System.err.println("Datadir " + dataDir.getAbsolutePath()
          + " kon niet aangemaakt worden!");
    }
  }

  private <T> T createBean(Class<? extends T> clazz) {
    Iterator<Bean<?>> iter = beanManager.getBeans(clazz).iterator();
    if (!iter.hasNext()) {
      logger.log("Class not found: " + clazz.getName());
      throw new IllegalStateException(
          "CDI BeanManager cannot find an instance of requested type "
              + clazz.getName());
    }
    @SuppressWarnings("unchecked")
    Bean<T> bean = (Bean<T>) iter.next();
    CreationalContext<T> ctx = beanManager.createCreationalContext(bean);
    @SuppressWarnings("unchecked")
    T beanInstance = (T) beanManager.getReference(bean, clazz, ctx);
    return beanInstance;
  }

  private static String[] createArgs(String[] args, Class<?> jobClazz) {
    List<String> params = new ArrayList<>(Arrays.asList(args));
    params.add(jobClazz.getName());
    return params.toArray(new String[params.size()]);
  }

  public static void run(Class<?> jobClazz, String[] args) {
    Properties props = System.getProperties();
    props.put("org.slf4j.simpleLogger.defaultLogLevel", "ERROR");
    System.setProperties(props);
    StartMain.main(JobStarter.createArgs(args, jobClazz));
  }
}
