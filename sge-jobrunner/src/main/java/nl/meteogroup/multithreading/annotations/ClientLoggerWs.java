package nl.meteogroup.multithreading.annotations;

import javax.websocket.ClientEndpoint;
import javax.websocket.OnMessage;
import javax.websocket.Session;

@ClientEndpoint
public class ClientLoggerWs {
  @OnMessage
  public void onMessage(String message, Session session) {
    System.out.println(message);
  }
}