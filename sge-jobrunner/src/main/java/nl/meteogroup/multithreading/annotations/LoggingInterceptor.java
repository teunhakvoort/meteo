package nl.meteogroup.multithreading.annotations;

import java.io.Serializable;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;

import nl.meteogroup.multithreading.core.JobLogger;

@Interceptor
@Log
public class LoggingInterceptor implements Serializable {

  private static final long serialVersionUID = 4457211814392518354L;

  @Inject
  JobLogger logger;

  public LoggingInterceptor() {

  }

  @AroundInvoke
  public Object logMethodEntry(InvocationContext invocationContext)
      throws Exception {
    String invocation = invocationContext.getMethod().getDeclaringClass()
        .getName()
        + "." + invocationContext.getMethod().getName();
    logger.log("+" + invocation);
    long start = System.currentTimeMillis();
    try {
      return invocationContext.proceed();
    } finally {
      logger.log("-" + invocation + " (" + (System.currentTimeMillis() - start)
          + " ms).");
    }
  }
}
