package runtime.starter;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class PortScanner extends PortScan {

   public boolean isFreePort(String machine, int port) throws UnknownHostException {
      return !isBusy(InetAddress.getByName(machine), port);
   }

}
