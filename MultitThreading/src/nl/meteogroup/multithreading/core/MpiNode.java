package nl.meteogroup.multithreading.core;

import nl.meteogroup.multithreading.domain.Context;

public interface MpiNode {

   public void runNode(Context context);

}
