package nl.meteogroup.multithreading.core;

import nl.meteogroup.multithreading.domain.Context;

public interface MpiMaster {

   public abstract void runMaster(Context context);

}
