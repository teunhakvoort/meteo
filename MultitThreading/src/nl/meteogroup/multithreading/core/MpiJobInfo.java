package nl.meteogroup.multithreading.core;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface MpiJobInfo {

   Class<? extends MpiMaster> master();

   Class<? extends MpiNode> node();
}
