package nl.meteogroup.multithreading.core;

import java.io.File;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.Properties;

import mpi.MPI;
import nl.meteogroup.multithreading.demo.domain.Messages;
import nl.meteogroup.multithreading.domain.Context;

public abstract class JobStarter {
   public void run(String[] args) throws Exception {
      try {
         System.out.println(Arrays.toString(args));

         File dataDir = new File(new File(".").getCanonicalFile(), getClass().getSimpleName() + "/"
               + Long.toString(System.currentTimeMillis()));
         // dataDir.mkdirs();
         MPI.Init(args);
         int myrank = MPI.COMM_WORLD.Rank();
         int size = MPI.COMM_WORLD.Size();
         if (myrank == 0) {
            // printEnvironmentInfo();
            dataDir.mkdirs();
            if (dataDir.exists()) {
               System.out.println("Datadir succesvol aangemaakt op: " + dataDir.getAbsolutePath());
            } else {
               System.err.println("Datadir " + dataDir.getAbsolutePath() + " kon niet aangemaakt worden!");
            }
         }
         MpiJobInfo jobInfo = getClass().getAnnotation(MpiJobInfo.class);
         Class<? extends MpiMaster> masterClazz = jobInfo.master();
         Class<? extends MpiNode> nodeClazz = jobInfo.node();

         MpiJob mpiRunner = new MpiJob();
         if (myrank == 0) {
            Context context = new Context(myrank, size, dataDir);
            System.out.println("Starting job on " + size + " cores.");
            System.out.println("Starting master " + context.getRang());

            MpiMaster master = masterClazz.getConstructor().newInstance();
            for (Integer node : context.getNodeIds()) {
               mpiRunner.sendObjectToNode(context, Messages.CONTEXT, node);
            }
            master.runMaster(context);

         } else {
            Context context = mpiRunner.recieveObject(Context.class, Messages.CONTEXT);
            System.out.println("Starting node " + context.getRang());
            InetAddress addr = InetAddress.getLocalHost();
            String ipAddress = addr.getHostAddress();
            System.out.println("Address: " + addr.getHostName() + "(" + ipAddress + ")");

            MpiNode node = nodeClazz.getConstructor().newInstance();
            node.runNode(context);
         }
      } finally {
         MPI.Finalize();
      }
   }

   private void printEnvironmentInfo() {
      Properties props = System.getProperties();
      props.list(System.out);
   }
}
