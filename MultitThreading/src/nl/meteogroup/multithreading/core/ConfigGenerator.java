package nl.meteogroup.multithreading.core;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

import runtime.starter.PortScanner;

public class ConfigGenerator {

   public static void main(String[] args) throws Exception {
      String jobId = args[0];
      String jobName = args[1];

      String machineName = null;
      int processes = 0;

      Path confPath = Paths.get("./" + jobId + ".conf");
      Path machinesPath = Paths.get("./" + jobId + ".machines.conf");
      if (Files.exists(confPath)) {
         return;
      }
      BufferedReader reader = Files.newBufferedReader(Paths.get("./" + jobName + "." + "po" + jobId),
            Charset.defaultCharset());

      BufferedWriter writer = Files.newBufferedWriter(confPath, Charset.defaultCharset());
      BufferedWriter machineFileWriter = Files.newBufferedWriter(machinesPath, Charset.defaultCharset());

      ArrayList<String> machines = new ArrayList<>();
      @SuppressWarnings("unused")
      String header = reader.readLine();
      while ((machineName = reader.readLine()) != null) {
         machines.add(machineName);
         processes++;
      }
      writer.write("# Number of processes\n");
      writer.write(processes + "\n");
      writer.write("# Protocol switch limit\n");
      writer.write("131072\n");
      writer.write("# Entry in the form of machinename@port@rank\n");

      int ranc = 0;
      int startport = 15000;
      PortScanner scanner = new PortScanner();
      for (String machine : machines) {
         while (true) {
            if (scanner.isFreePort(machine, startport)) {
               break;
            } else {
               startport++;
            }
         }

         writer.write(machine + "@" + startport + "@" + ranc + "\n");
         ranc++;
         startport += 2;
      }

      for (String machine : machines) {
         machineFileWriter.write(machine + "\n");
      }
      writer.close();
      reader.close();
      machineFileWriter.close();

   }
}
