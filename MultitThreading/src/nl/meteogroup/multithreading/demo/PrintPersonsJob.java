package nl.meteogroup.multithreading.demo;

import nl.meteogroup.multithreading.core.JobStarter;
import nl.meteogroup.multithreading.core.MpiJobInfo;

@MpiJobInfo(master = PrintPersonsJobMaster.class, node = PrintPersonsJobNode.class)
public class PrintPersonsJob extends JobStarter {

   public static void main(String[] args) {
      try {
         new PrintPersonsJob().run(args);
      } catch (Exception e) {
         System.out.println("Job starten mislukt!");
      }
   }

}
