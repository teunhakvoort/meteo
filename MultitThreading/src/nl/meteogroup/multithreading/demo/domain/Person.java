package nl.meteogroup.multithreading.demo.domain;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlAccessorType(XmlAccessType.FIELD)
public class Person implements Serializable {

   private static final long serialVersionUID = 38983591360515484L;

   @Override
   public String toString() {
      return "Person [name=" + name + ", id=" + id + ", adress=" + adress + "]";
   }

   public Person() {

   }

   public Person(String name, String adress, Long id) {
      this.name = name;
      this.adress = adress;
      this.id = id;
   }

   @XmlAttribute
   private String name;
   @XmlAttribute
   private Long   id;
   @XmlAttribute
   private String adress;
   @XmlAttribute
   private int    visitCounter = 0;

   public void incrementVisitCounter() {
      visitCounter++;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getAdress() {
      return adress;
   }

   public void setAdress(String adress) {
      this.adress = adress;
   }

}
