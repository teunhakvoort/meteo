package nl.meteogroup.multithreading.demo.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PersonList {

   @XmlElementWrapper
   private List<Person> persons = new ArrayList<Person>();

   public PersonList() {
   }

   public void addAll(Collection<Person> persons) {
      this.persons.addAll(persons);
   }
}
