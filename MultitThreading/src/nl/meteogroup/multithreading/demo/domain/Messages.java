package nl.meteogroup.multithreading.demo.domain;

import nl.meteogroup.multithreading.domain.Message;

public class Messages {
   public final static Message CONTEXT             = new Message(10, "Context");

   public final static Message LIST_PERSONS        = new Message(100, "Lijst met personen");

   public final static Message LIST_PERSONS_RESULT = new Message(200, "Lijst met personen resultaat");
}
