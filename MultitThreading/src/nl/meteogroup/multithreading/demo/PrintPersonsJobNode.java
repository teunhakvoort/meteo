package nl.meteogroup.multithreading.demo;

import java.util.ArrayList;

import nl.meteogroup.multithreading.core.MpiJob;
import nl.meteogroup.multithreading.core.MpiNode;
import nl.meteogroup.multithreading.demo.domain.Messages;
import nl.meteogroup.multithreading.demo.domain.Person;
import nl.meteogroup.multithreading.domain.Context;

public class PrintPersonsJobNode extends MpiJob implements MpiNode {

   @Override
   public void runNode(Context context) {
      try {
         @SuppressWarnings("unchecked")
         ArrayList<Person> persons = recieveObject(ArrayList.class, Messages.LIST_PERSONS);

         for (Person person : persons) {
            person.incrementVisitCounter();
         }
         System.out.println("Send result back");
         sendObjectToMaster(persons, Messages.LIST_PERSONS_RESULT);

      } catch (Exception e) {
         System.out.println("Ontvangen mislukt");
      }
   }
}
