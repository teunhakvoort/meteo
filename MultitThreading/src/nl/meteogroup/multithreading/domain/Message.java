package nl.meteogroup.multithreading.domain;

public class Message {
   private int    id;
   private String description;

   public Message(int id, String description) {
      this.id = id;
      this.description = description;
   }

   public String getDescription() {
      return description;
   }

   public int getId() {
      return id;
   }
}
