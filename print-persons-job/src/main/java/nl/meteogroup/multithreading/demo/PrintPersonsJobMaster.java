package nl.meteogroup.multithreading.demo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import javax.inject.Inject;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import nl.meteogroup.multithreading.annotations.Log;
import nl.meteogroup.multithreading.core.JobLogger;
import nl.meteogroup.multithreading.core.Messager;
import nl.meteogroup.multithreading.core.MpiMaster;
import nl.meteogroup.multithreading.demo.domain.Messages;
import nl.meteogroup.multithreading.demo.domain.Person;
import nl.meteogroup.multithreading.demo.domain.PersonList;
import nl.meteogroup.multithreading.domain.Context;
import nl.meteogroup.multithreading.domain.SendException;

public class PrintPersonsJobMaster implements MpiMaster {

  @Inject
  Messager messager;

  @Inject
  JobLogger logger;

  @Override
  @Log
  public void runMaster(Context context) {
    logger.log("Start master...");

    ArrayList<Person> persons = new ArrayList<Person>();
    for (int i = 0; i < context.getNumberOfNodes(); i++) {
      Person p = new Person("jan", "straat 1", Long.valueOf(i));
      persons.add(p);
    }
    try {
      Iterator<Integer> nodeIds = context.getNodeIds().iterator();
      for (int i = 0; i < context.getNumberOfNodes(); i++) {
        messager.sendObjectToNode(
            new ArrayList<Person>(persons.subList(i, i + 1)),
            Messages.LIST_PERSONS, nodeIds.next());
      }

      PersonList personList = new PersonList();
      for (Integer node : context.getNodeIds()) {
        try {
          System.out.println("Waiting for result from node " + node);
          ArrayList<Person> resultFromNode = messager.recieveObjectFromNode(
              ArrayList.class, Messages.LIST_PERSONS_RESULT, node);
          System.out.println("SubResult: " + resultFromNode.size());
          personList.addAll(resultFromNode);
        } catch (Exception e) {
          System.err.println(e.getMessage());
          e.printStackTrace();
        }
      }

      JAXBContext jaxBcontext = JAXBContext.newInstance(Person.class,
          PersonList.class);
      Marshaller marshaller = jaxBcontext.createMarshaller();
      FileOutputStream output = new FileOutputStream(new File(
          context.getDataDir(), "output.xml"));
      try {
        marshaller.marshal(personList, output);
      } finally {
        output.close();
      }
    } catch (SendException e) {
      System.out.println("Versturen mislukt");
    } catch (JAXBException e) {
      System.err.println("Niet gelukt om XML te maken: " + e.getMessage());
    } catch (IOException e) {
      System.err.println("Kan file niet wegschrijven: " + e.getMessage());
    }
  }

}
