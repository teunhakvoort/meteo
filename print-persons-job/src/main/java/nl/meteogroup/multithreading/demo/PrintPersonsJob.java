package nl.meteogroup.multithreading.demo;

import nl.meteogroup.multithreading.core.JobStarter;
import nl.meteogroup.multithreading.core.MpiJobInfo;

@MpiJobInfo(master = PrintPersonsJobMaster.class, node = PrintPersonsJobNode.class)
public class PrintPersonsJob {

  public static void main(String[] args) {
    JobStarter.run(PrintPersonsJob.class, args);
  }

}
