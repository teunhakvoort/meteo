package nl.meteogroup.multithreading.demo;

import java.util.ArrayList;

import javax.inject.Inject;

import nl.meteogroup.multithreading.core.Messager;
import nl.meteogroup.multithreading.core.MpiNode;
import nl.meteogroup.multithreading.demo.domain.Messages;
import nl.meteogroup.multithreading.demo.domain.Person;
import nl.meteogroup.multithreading.domain.Context;

public class PrintPersonsJobNode implements MpiNode {

  @Inject
  Messager messager;

  @Override
  public void runNode(Context context) {
    try {
      @SuppressWarnings("unchecked")
      ArrayList<Person> persons = messager.recieveObject(ArrayList.class,
          Messages.LIST_PERSONS);

      for (Person person : persons) {
        person.incrementVisitCounter();
      }
      System.out.println("Send result back");
      messager.sendObjectToMaster(persons, Messages.LIST_PERSONS_RESULT);

    } catch (Exception e) {
      System.out.println("Ontvangen mislukt");
    }
  }
}
